from flask import Flask
from flask import render_template

app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template('render.html')

@app.route('/<diseasename>')
def default_disease(diseasename):
    return render_template('render.html', diseasename=diseasename)

@app.route('/disease/<diseasename>')
def render_search(diseasename):
    return render_template('disease.html', diseasename=diseasename)

if __name__ == '__main__':
    app.run()
