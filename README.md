# README #

This document contains instructions for you to get the IDOL KG and the frontend application up and running. 

![KG](https://drive.google.com/open?id=15VO9e5DeTc_ZvUbsZekCz2hWvc3MO7IC)

### Stack ###

* IDOL Knowledge Graph
* Version 11.6
* Python 2.7
* Flask web application for rendering the graph
* Cytoscape js
* Latest jquery


### How do I get set up? ###

* Summary of set up
Ensure that the IDOL KG 11.6 is installed and running. Copy the Astellas.bin file into the graph root directory. 
* Configuration
Change the configuration so that the following config parameter is set in knowledgegraph.cfg
[Persistence]
filename=astellas.bin


### Who do I talk to? ###

* Vinay Joseph (vinay@vinayjoseph.com)
