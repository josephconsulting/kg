var selected_source_node_text;
var selected_target_node_text;

function setSourceNodeText(text){
    if (text == ""){
        $('#divSourceNode').html("");
        $('#divCount').html("");
    }
    else{
        $('#divSourceNode').html("<strong>Sponsor : </strong> "+text);
        selected_source_node_text = text;
    }

}
function setTargetNodeText(text){
    if (text == ""){
        $('#divTargetNode').html("");
        $('#divCount').html("");
    }
    else{
        $('#divTargetNode').html("<strong>Disease : </strong> "+text);
        selected_target_node_text = text;
    }

}
function setCount(count){
    if (count == 0){
        $('#divCount').empty();
        $('#studies').empty();
        $('#studiesList').empty();
        $('#divSourceNode').empty();
        $('#divTargetNode').empty();
    }
    else{
        $('#divCount').html("<strong>Number of trials : </strong>"+count);
    }

}
function search(term){
    var url = "";
    if (term == ""){
        url = "http://localhost:10300/action=GetNeighborhood&NodeNames=Astellas Pharma Inc&Method=Count&MaxResults=30";
    }
    else
    {
        url = "http://localhost:10300/action=GetNeighborhood&NodeNames=Astellas Pharma Inc,"+term+"&Method=Count&MaxResults=30";
    }
    $.ajax({
        type: "GET",
        url: url,
        datatype: "xml",
        success: function(xml) {
            console.log(xml);
            parse(xml);
        }
    });
}

$(document).ready(function() {
    var this_js_script = $("#custom");
    var disease_name = this_js_script.attr('disease');
    search(disease_name);
});


function parse(xml){
    var i;
    var nodes = xml.getElementsByTagName("node");
    console.log("node length = " + nodes.length);
    var edges = xml.getElementsByTagName("edge");
    console.log("edges length = " + edges.length);
    var node_elements = [];
    var edge_elements = [];
    for (i=0; i< nodes.length; i++) {
        node_id = nodes[i].getAttribute("id");
        node_name = nodes[i].firstElementChild.firstElementChild.textContent;
        console.log("node, id = " + node_name + ", " + node_id);
        var data = { "data": {"id": node_id, "name": node_name, "color":"yellow" }};
        node_elements.push(data);
    }
    for (i=0; i<edges.length; i++){
        source = edges[i].getAttribute('source');
        target = edges[i].getAttribute('target');
        count = edges[i].firstElementChild.firstElementChild.textContent;
        type = edges[i].firstElementChild.lastElementChild.textContent;
        var data = {"data": {"id":source+target, "source":source, "target": target, "count":count, "type":type}};
        for (x=0; x< node_elements.length; x++){
            node = node_elements[x];
            if(node["data"]["id"]==target){
                node_elements[x]["data"]["color"]="red";
                console.log("Changing ..." + node_elements[x]);
            }
            if(node["data"]["name"]=="Astellas Pharma Inc"){
                node_elements[x]["data"]["color"]="green";
            }
        }
        edge_elements.push(data);
    }
    var total_nodes = node_elements.concat(edge_elements);
    console.log(total_nodes);
    var options = {
        name: 'circle',
        fit: true, // whether to fit the viewport to the graph
        padding: 30, // the padding on fit
        boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
        avoidOverlap: true, // prevents node overlap, may overflow boundingBox and radius if not enough space
        nodeDimensionsIncludeLabels: false, // Excludes the label when calculating node bounding boxes for the layout algorithm
        spacingFactor: undefined, // Applies a multiplicative factor (>0) to expand or compress the overall area that the nodes take up
        radius: undefined, // the radius of the circle
        startAngle: 3 / 2 * Math.PI, // where nodes start in radians
        sweep: undefined, // how many radians should be between the first and last node (defaults to full circle)
        clockwise: true, // whether the layout should go clockwise (true) or counterclockwise/anticlockwise (false)
        sort: undefined, // a sorting function to order the nodes; e.g. function(a, b){ return a.data('weight') - b.data('weight') }
        animate: false, // whether to transition the node positions
        animationDuration: 500, // duration of animation in ms if enabled
        animationEasing: undefined, // easing of animation if enabled
        animateFilter: function ( node, i ){ return true; }, // a function that determines whether the node should be animated.  All nodes animated by default on animate enabled.  Non-animated nodes are positioned immediately when the layout starts
        ready: undefined, // callback on layoutready
        stop: undefined, // callback on layoutstop
        transform: function (node, position ){ return position; } // transform a given node position. Useful for changing flow direction in discrete layouts
    };
    var cy = cytoscape({
    container: document.getElementById('cy'),
    elements: total_nodes ,
    style:[
        {
            selector: 'node',
            style:{
                'background-color':'data(color)',
                'font-size':'10px',
                'label':'data(name)'
            }
    },{
    selector: 'edge',
        style:{
            'width':'data(count)',
            'line-color':'#c2e8c8',
            'target-arrow-color':'black',
            'target-arrow-shape':'triangle'
        }
    }],
        layout:{
            name: 'circle',
            fit: true, // whether to fit the viewport to the graph
            padding: 30, // the padding on fit
            boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
            avoidOverlap: true, // prevents node overlap, may overflow boundingBox and radius if not enough space
            nodeDimensionsIncludeLabels: false, // Excludes the label when calculating node bounding boxes for the layout algorithm
            spacingFactor: undefined, // Applies a multiplicative factor (>0) to expand or compress the overall area that the nodes take up
            radius: 100, // the radius of the circle
            startAngle: 3 / 2 * Math.PI, // where nodes start in radians
            sweep: undefined, // how many radians should be between the first and last node (defaults to full circle)
            clockwise: true, // whether the layout should go clockwise (true) or counterclockwise/anticlockwise (false)
            sort: undefined, // a sorting function to order the nodes; e.g. function(a, b){ return a.data('weight') - b.data('weight') }
            animate: true, // whether to transition the node positions
            animationDuration: 500, // duration of animation in ms if enabled
            animationEasing: undefined, // easing of animation if enabled
            animateFilter: function ( node, i ){ return true; }, // a function that determines whether the node should be animated.  All nodes animated by default on animate enabled.  Non-animated nodes are positioned immediately when the layout starts
            ready: undefined, // callback on layoutready
            stop: undefined, // callback on layoutstop
            transform: function (node, position ){ return position; }
        }
    });
    var position_of_a_moved_node;
    cy.on('mouseup', function (event) {
        var evtTarget = event.target;

      if( evtTarget === cy ){
      } else {
        evtTarget.neighborhood('edge').style({'line-color':'#c2e8c8'});
        evtTarget.connectedEdges().style({'line-color':'#c2e8c8'});
      }
    });
    cy.on('')
    cy.on('mousedown', function(event){
      // target holds a reference to the originator
      // of the event (core or element)
      var evtTarget = event.target;
      var selected_edge_source_node;
      var selected_edge_target_node;
      if (evtTarget.isNode()){
        var colour = evtTarget[0]._private.data.color;
        var name = evtTarget[0]._private.data.name;
        evtTarget.neighborhood('edge').style({'line-color':'#4F7942'});
        evtTarget.connectedEdges().style({'line-color':'#4F7942'});
        setCount(0);
      }
      if (evtTarget.isEdge()){
          var x = 0;
          var source_id = evtTarget[0]._private.data.source;
          var target_id = evtTarget[0]._private.data.target;
          var count = evtTarget[0]._private.data.count;
          $.when(getNodeName(source_id),getNodeName(target_id)).then(function (xml1, xml2) {
              var text1 = process_nodeName(xml1[0], source_id);
              setSourceNodeText(text1);
              var text2 = process_nodeName(xml2[0], target_id);
              setTargetNodeText(text2);

            console.log("Both source and target is set to " + text1 + " and " + text2);
            getStudies(text1, text2);
          });
      }
      if( evtTarget === cy ){
      } else {


      }
    });

}

function getStudies(agency, condition){
    var agency_encode = encodeURIComponent(agency);
    var condition_encode = encodeURIComponent(condition);
    var fieldText = "MATCH{"+condition_encode+"}:CLINICAL_STUDY_CONDITION+AND+MATCH{"+agency_encode+"}:CLINICAL_STUDY_SPONSORS_LEAD_SPONSOR_AGENCY";
    var url = "http://localhost:9100/?";
    console.log("URL To investigate " + url);
    return $.ajax({
        data: {
            action:'query',
            text: '*',
            combine:'simple',
            databasematch:"CTGOV",
            fieldtext: fieldText,
            minscore:0,
            maxresults:30,
            print:'all',
            querysummary:true,
            querysummarylength:50
        },
        type:"GET",
        url:url,
        datatype:"xml",
        success: function (xml) {
            console.log(xml);
            var hits = xml.getElementsByTagName("autn:hit");
            setCount(hits.length);
            if (hits.length > 0){
                $('#studies').html("<ul id='studiesList'></ul>");
                for(i =0; i<hits.length; i++){
                var title = hits[i].childNodes[5].textContent;
                $('#studiesList').append("<li>"+title+"</li>");
                }
            }
            if (hits.length == 0){
                $('#studies').empty();
            }
        }
    });
    

}

function getNodeName(id){
    var url = "http://localhost:10300/a=getsubgraph&nodeids="+id;
    return $.ajax({
        type: "GET",
        url: url,
        datatype: "xml"
    });
}

function process_nodeName(xml, id){
    if (typeof(id) !== "undefined"){
        var nodes = xml.getElementsByTagName("nodes");
        var text = nodes[0].getElementsByTagName('node')[0].textContent;
        return text;
    }
    else{
        return "";
    }

}